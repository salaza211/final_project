
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String requester = "";
        String password = "";
        String product = "";
        int amount = 0;
        int toRun = 0;

        try {
            requester = request.getParameter("requester");
            password = request.getParameter("password");
            product = request.getParameter("product");
            amount = Integer.parseInt(request.getParameter("amount"));
        } catch (Exception e) {
            toRun = 1;
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">Some of the information fields were empty.</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }

        if (amount < 1 & toRun == 0){
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">The amount must be more than 0, please enter a higher amount of product.</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }

        CheckUser check = new CheckUser();
        boolean auth = check.CheckUser(requester, password);
        if (auth == false & amount > 0) {
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">Wrong Username and Password</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }

        checkAccess check1 = new checkAccess();
        boolean auth1 = check1.CheckAccess(requester);
        if (auth1 == false & auth == true & amount > 0) {
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">You do not have access to order, please request access.</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }

        checkInventory check3 = new checkInventory();
        boolean inventory_check = check3.CheckInventory(product);

        if (inventory_check == false & auth1 == true & auth == true & amount > 0) {
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">There is no product with that name.</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }

        checkAmount check4 = new checkAmount();
        boolean amount_check = check4.CheckAmount(amount, product);

        if (amount_check == false & inventory_check == true & auth1 == true & auth == true & amount > 0) {
            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Error!</h2>");
            out1.println("<p class=\"title\">There is not enough of the product selected.</p><br/>");
            out1.println("<p class=\"title\">You will be redirected to the form page in 7 seconds.</p><br/>");
            out1.println("<meta http-equiv=\"refresh\" content=\"5; URL=http://localhost:8080/final_project_war/\" />");
            out1.println("</body></html>");
        }


        if (auth == true & auth1 == true & inventory_check == true & amount_check == true & amount > 0) {
            Orders order = new Orders(requester, product, amount);
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();

            entityManager.persist(order);
            entityManager.getTransaction().commit();

            entityManagerFactory.close();

            PrintWriter out1 = response.getWriter();
            response.setContentType("text/html");
            out1.println("<html><head><link href=\"css/main.css\" rel=\"stylesheet\"></head><body>");
            out1.println("<div class=\"page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins\"> <div class=\"wrapper wrapper--w780\">");
            out1.println("<div class=\"card card-3\"> <div class=\"card-body\"><h2 class=\"title\">Order Summary</h2>");
            out1.println("<p class=\"title\">Ordered By: " + requester + " </p><br/>");
            out1.println("<p class=\"title\">Product ordered: " + product + " </p><br/>");
            out1.println("<p class=\"title\">Amount Ordered: " + amount + " </p><br/>");
            out1.println("</body></html>");

        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }
}
