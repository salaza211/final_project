import com.enzoic.client.Enzoic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        int myInput = 5 ;
        System.out.println("Welcome to Razalas Industries Orders and Inventory Database");

        while (myInput != 0) {
            System.out.println("----------------------------------------------------");
            System.out.println("Press 1 to create a new order.");
            System.out.println("Press 2 to obtain a list of all orders.");
            System.out.println("Press 3 to create a new user for orders.");
            System.out.println("Press 4 to create inventory.");
            System.out.println("Press 0 to quit.");
            System.out.println("----------------------------------------------------");
            Scanner obj = new Scanner(System.in);
            myInput = obj.nextInt();


            while (myInput == 1) {

                Scanner in = new Scanner(System.in);
                Scanner obj1 = new Scanner(System.in);

                System.out.println("Please enter your username: ");
                String requester = in.nextLine();

                System.out.println("Please enter your password: ");
                String password = in.nextLine();

                CheckUser check = new CheckUser();
                boolean auth = check.CheckUser(requester, password);
                if (auth == false) {
                    System.out.println("Wrong username or password: ");
                    break;
                }

                checkAccess check2 = new checkAccess();
                boolean auth2 = check2.CheckAccess(requester);
                if (auth2 == false) {
                    System.out.println("You do not have access, please request it or use another account. ");
                    break;
                }

                System.out.println("Welcome: " + requester);

                System.out.println("Please enter the product to be ordered: ");
                String product = in.nextLine();

                checkInventory check3 = new checkInventory();
                boolean inventory_check = check3.CheckInventory(product);

                if (inventory_check == false) {
                    System.out.println("There is no product with that name. ");
                    break;
                }

                System.out.println("Please enter the amount of product to be ordered: ");
                int amount = obj1.nextInt();

                checkAmount check4 = new checkAmount();
                boolean amount_check = check4.CheckAmount(amount, product);

                if (amount_check == true) {
                    System.out.println("There is not enough of the product selected. ");
                    break;
                }


                Orders order = new Orders(requester, product, amount);
                EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();

                entityManager.persist(order);
                entityManager.getTransaction().commit();

                entityManagerFactory.close();

                decreaseInventory di = new decreaseInventory();
                di.DecreaseInventory(product, amount);
                myInput = 5;
            }
            if (myInput == 2) {
                EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();
                List<Orders> ordersList = entityManager.createNativeQuery("SELECT * FROM Orders ;", Orders.class).getResultList();
                entityManagerFactory.close();
                for (Orders orders : ordersList) {
                    System.out.println(orders.getIdOrders() + " || " + orders.getRequester() + " || " + orders.getProduct_Name() + " || " + orders.getProduct_Amount());
                }
                myInput = 5;
            }

            while (myInput == 3) {
                Scanner in = new Scanner(System.in);
                Scanner obj1 = new Scanner(System.in);

                System.out.println("Please enter your username: ");
                String requester = in.nextLine();

                System.out.println("Please enter your password: ");
                String password1 = in.nextLine();

                CheckUser check = new CheckUser();
                boolean auth = check.CheckUser(requester, password1);
                if (auth == false) {
                    System.out.println("Wrong username or password: ");
                    break;
                }

                checkAccess check2 = new checkAccess();
                boolean auth2 = check2.CheckAccess(requester);
                if (auth2 == false) {
                    System.out.println("You do not have access, please request it or use another account. ");
                    break;
                }

                System.out.println("Please enter the new user name: ");
                String name = in.nextLine();
                System.out.println("Please enter the new user lastname: ");
                String last_name = in.nextLine();
                System.out.println("Please enter the new users username: ");
                String username = in.nextLine();
                System.out.println("Please enter the new users password: ");
                String password = in.nextLine();

                checkPassword check4 = new checkPassword();
                boolean pass_check = check4.CheckPassword(password);
                if (pass_check == false) {
                    System.out.println("*****Your password is too weak, try again.****** ");
                    break;
                }

                System.out.println("Should this user be able to alter this database? Enter 1 for yes, 0 for no: ");
                int access_query = obj1.nextInt();


                Users users = new Users(name, last_name, username, password, access_query);
                EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();
                entityManager.persist(users);
                entityManager.getTransaction().commit();
                entityManagerFactory.close();
                myInput = 5;

            }

            while (myInput == 4) {
                Scanner in = new Scanner(System.in);
                Scanner obj1 = new Scanner(System.in);

                System.out.println("Please enter your username: ");
                String requester = in.nextLine();

                System.out.println("Please enter your password: ");
                String password = in.nextLine();

                CheckUser check = new CheckUser();
                boolean auth = check.CheckUser(requester, password);
                if (auth == false) {
                    System.out.println("Wrong username or password: ");
                    break;
                }

                System.out.println("Welcome: " + requester);

                checkAccess check2 = new checkAccess();
                boolean auth2 = check2.CheckAccess(requester);
                if (auth2 == false) {
                    System.out.println("You do not have access, please request it or use another account. ");
                    break;
                }

                System.out.println("Please the new product's name: ");
                String product = in.nextLine();
                System.out.println("Please enter the stock for the product: ");
                int stock = obj1.nextInt();
                System.out.println("Please enter the price for the product: ");
                int price = obj1.nextInt();


                Inventory inventory = new Inventory(product, stock, price);
                EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
                EntityManager entityManager = entityManagerFactory.createEntityManager();
                entityManager.getTransaction().begin();
                entityManager.persist(inventory);
                entityManager.getTransaction().commit();
                entityManagerFactory.close();
                myInput = 5;

            }
        }
    }

}

